import socket
import subprocess
import os

# create TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
FILENAME = "one.jpg"
LAST_FILE = "four.png"
MAX_ITERS = 592
# retrieve local hostname

# bind the socket to the port 23456, and connect
server_address = ("192.168.50.3", 31002)  
# server_address = ("localhost", 31008)  

def prepareData(msg):
    subprocess.run(["./exif-steg/exiftool", "-artist=%s" % msg,  FILENAME])
    data = open(FILENAME, "rb").read()
    return data

# define example data to be sent to the server
sock.connect(server_address) 
text = input("-> ")
data = prepareData(text)
sock.sendall(data)
data = sock.recv(1024)
i = 0
while (data):
    i += 1
    fl = open(LAST_FILE, "ab")
    fl.write(data)
    data = sock.recv(1024)
    # print(data)
sock.close() 
fl.detach()

# sys.l
cmd = "java -jar decode.jar 1337228 https://d36jcksde1wxzq.cloudfront.net/be7833db9bddb4494d2a7c3dd659199a.png " + LAST_FILE
# print(cmd)
res = subprocess.call(cmd.split(' '))
print(res)
subprocess.run(["rm", "four.png"])

# close connection
 